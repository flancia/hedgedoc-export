#!/bin/bash
# requires https://github.com/hackmdio/hackmd-cli#hackmd-cli-list
# $ npm install @hackmd/hackmd-cli
# 
# almost uses https://github.com/simonthum/git-sync although for our use it *probably* isn't needed and we can just push? let's see.
#
# unfortunately hedgedoc doesn't have an easy way to list all notes (!), so we need to get the list from the database.
cd ~/export
export CMD_CLI_SERVER_URL=https://doc.anagora.org
# this is expected to already be a git repo cloned from https://github.com/flancia-coop/doc.anagora.org.
DIR=doc.anagora.org
mkdir -p $DIR

psql < list.sql # this outputs to l.txt
while read note; do
	echo "exporting ${note} to ${DIR}/${note}.md"
	~/node_modules/@hackmd/hackmd-cli/bin/run export "${note}" "${DIR}/${note}.md" < /dev/null
done < l.txt
cd ${DIR}
git add "*md"
git commit -a -m "stoa update"
git push
# ../git-sync/git-sync sync
