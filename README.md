# hedgedoc-export

This repository contains a very simple script, you could say a hack, to export all the content of a Hedgedoc instance to a set of Markdown files, which are then versioned in a separate git repository.

Enjoy if you find this useful, but otherwise feel free to ignore :D
